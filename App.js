import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

class App extends Component {

  componentWillMount() {
    // const { navigation } = this.props;
    // const { navigate } = navigation;
    this.props.navigation.navigate('HostBottomStack');
  }
  render() {
    return (
      <View style={{ flex: 1 }} />
    );
  }
}
export default App;
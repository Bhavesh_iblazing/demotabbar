import React from 'react';
import { View, Text } from 'react-native';
import {
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
} from 'react-navigation';
import { Icon } from 'react-native-elements';

//Relative Imports
import { IS_LOGIN } from './Helper/Constants'

// Auth Loading Screen
import App from './App';

// App Screens
import Login from './src/Screens/Login';
import FindAPool from './src/Screens/FindAPool';
import Reservations from './src/Screens/Reservations';
import Messages from './src/Screens/Messages';
import People from './src/Screens/People';
import HostScreen from './src/Screens/Host';

const tabIcon = ({ name, tintColor, type, size, showBadge }) => <View>
  <Icon name={name} type={type} color={tintColor} size={size} />
  {showBadge ?
    <View style={{position: 'absolute', right: -10, top: 0, width: 20, height: 20, backgroundColor: '#f00', borderRadius: 10,justifyContent: 'center', alignItems: 'center'  }}>
    <Text style={{  color: '#fff', fontSize: 12  }}>3</Text> 
  </View>: null}
</View>;

const AppLoaderStack = createStackNavigator({
  App
})

const LoginStack = createStackNavigator({
  Login
})

const FindAPoolStack = createStackNavigator({
  // Write here Navigation Screen names
  FindAPool,

}, {
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: '#000',
      }
    })
  });

FindAPoolStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
    animationEnabled: false,
    tabBarIcon: ({ tintColor }) => tabIcon({ name: 'social-dropbox', tintColor: IS_LOGIN ? tintColor : 'red', type: 'simple-line-icon', size: 25, showBadge: false }),
  };
};

const ReservationStack = createStackNavigator({
  // Write here Navigation Screen names
  Reservations,

}, {
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: '#000',
      }
    })
  });

ReservationStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
    animationEnabled: false,
    tabBarIcon: ({ tintColor }) => tabIcon({ name: 'hearto', tintColor, type: 'antdesign', size: 25, showBadge: false }),
  };
};

const MessagesStack = createStackNavigator({
  // Write here Navigation Screen names
  Messages,

}, {
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: '#000',
      }
    })
  });

MessagesStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
    animationEnabled: false,
    tabBarIcon: ({ tintColor }) => tabIcon({ name: 'mail', tintColor, type: 'feather', size: 25, showBadge: true }),
  };
};

const PeopleStack = createStackNavigator({
  // Write here Navigation Screen names
  People,

}, {
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: '#000',
      }
    })
  });

PeopleStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
    animationEnabled: false,
    tabBarIcon: ({ tintColor }) => tabIcon({ name: 'user', tintColor, type: 'evilicon', size: 35, showBadge: false }),
  };
};

const Host = createStackNavigator({
  // Write here Navigation Screen names
  HostScreen,

}, {
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: '#000',
      }
    })
  });

Host.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
    animationEnabled: false,
    tabBarIcon: ({ tintColor }) => tabIcon({ name: 'user', tintColor, type: 'evilicon', size: 35, showBadge: false }),
  };
};

// without login Bottom bar design
const withOutLoginStack = createBottomTabNavigator(
  {
    'FIND A POOL': FindAPoolStack,
    RESERVATIONS: ReservationStack,
    MESSAGES: MessagesStack,
    PEOPLE: PeopleStack,
  },
  {
    initialRouteName: 'FIND A POOL',
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: 'gray',
      inactiveTintColor: 'gray',
      // activeBackgroundColor: 'red',
      // inactiveBackgroundColor: 'green',
      showLabel: true,
    }
  }
);

// With Login Bottom bar
const withLoginStack = createBottomTabNavigator(
  {
    'FIND A POOL': FindAPoolStack,
    RESERVATIONS: ReservationStack,
    MESSAGES: MessagesStack,
    PEOPLE: PeopleStack,
  },
  {
    initialRouteName: 'FIND A POOL',
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: 'blue',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: 'white',
      showLabel: true,
    }
  }
);

// WIth Login Bottom bar
const HostBottomStack = createBottomTabNavigator(
  {
    'FIND A POOL': FindAPoolStack,
    Reservation: ReservationStack,
    MESSAGES: MessagesStack,
    HOST: Host,
    PEOPLE: PeopleStack,
  },
  {
    initialRouteName: 'FIND A POOL',
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: 'blue',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: 'white',
      showLabel: true,
    }
  }
);

export default createSwitchNavigator(
  {
    AppLoading: AppLoaderStack,
    withLoginStack: withLoginStack,
    HostBottomStack: HostBottomStack,
    withOutLogin: withOutLoginStack,
    LoginStack: LoginStack,
  },
  {
    initialRouteName: 'withOutLogin',
  }
);
import React, { Component } from 'react';
import { View } from 'react-native';

class Host extends Component {
  static navigationOptions = {
    header: null,
  }
  render() {
    const { container } = styles;
    return (
      <View style={container} />
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#c3c3c3',
  }
}

export default Host

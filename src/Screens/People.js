import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

// Relative imports
import * as Constants from '../../Helper/Constants'

class People extends Component {
  static navigationOptions = {
    header: null,
  }
  render() {
    const { container, btnStyle, txtInfoStyle } = styles;
    return (
      <View style={[container, { backgroundColor: Constants.IS_LOGIN ? 'yellow' : 'white', }]}>
        {Constants.IS_LOGIN === false && (
          <View style={{ alignItems: 'center' }}>
            <Text style={txtInfoStyle}>You must login to operate this functionality</Text>
            <TouchableOpacity
              onPress={() => {
                Constants.IS_LOGIN = false
                this.props.navigation.navigate('LoginStack')
              }}
              style={btnStyle}
            >
              <Text style={{ fontSize: 18 }}>Login</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnStyle: {
    width: 150,
    height: 40,
    borderRadius: 20,
    borderWidth: 1.0,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  txtInfoStyle: {
    marginBottom: 15,
    marginHorizontal: 20,
    fontSize: 20,
    textAlign: 'center'
  }
}

export default People

import React, { Component } from 'react'
import { View, TouchableOpacity, Text } from 'react-native';

// Relative imports
import * as Constants from '../../Helper/Constants'

class Login extends Component {
  static navigationOptions = {
    header: null,
  }
  render() {
    const { container, btnStyle } = styles;
    return (
      <View style={container}>
        <TouchableOpacity
          onPress={() => {
            Constants.IS_LOGIN = true
            this.props.navigation.navigate('withLoginStack')
          }}
          style={btnStyle}
        >
          <Text>Swimmer Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            Constants.IS_LOGIN = true
            this.props.navigation.navigate('HostBottomStack')
          }}
          style={[btnStyle, { marginTop: 20 }]}
        >
          <Text>Host Login</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnStyle: {
    width: 200,
    height: 50,
    borderRadius: 25,
    borderWidth: 1.0,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
  }
}

export default Login
